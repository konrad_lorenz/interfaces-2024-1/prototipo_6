# Taller: Creación de un Servicio de Análisis de Sentimientos para Textos Largos

## 1. Objetivo del Taller
Desarrollar un servicio que evalúe sentimientos en textos de más de 1000 tokens, incluyendo una interfaz gráfica y la capacidad de evaluar comentarios en redes sociales de la Universidad Konrad Lorenz.

## 2. Desglose del Proyecto
El proyecto se divide en las siguientes partes:
- **Análisis de Sentimientos**: Uso de modelos de procesamiento de lenguaje natural para analizar el sentimiento de los textos.
- **Interfaz Gráfica de Usuario (GUI)**: Diseño de una interfaz amigable para ingresar textos y mostrar resultados.
- **Integración con Redes Sociales**: Conexión con APIs de redes sociales para analizar comentarios directamente.
- **Filtro de Publicación**: Sistema que recomienda si un comentario debe ser publicado basado en el análisis de sentimiento.

## 3. Herramientas y Tecnologías
- **Python**: Uso de FastApi o Django para la API del backend.
- **Html, css, js**: Para el desarrollo del frontend de la interfaz de usuario.
- **Transformers de Hugging Face**: Para implementar modelos de NLP que manejen textos largos.
- **APIs de redes sociales**: Para la integración con plataformas como Twitter, Facebook, etc (tambien puede ser fake/mock).

## 4. Estructura del Taller
- **Sesión 1**: Introducción a NLP y análisis de sentimientos.
- **Sesión 2**: Diseño y desarrollo de la API backend.
- **Sesión 3**: Creación de la interfaz gráfica y conexión con el backend.
- **Sesión 4**: Integración con redes sociales (tambien puede ser fake/mock).
- **Sesión 5**: Implementación de filtros de publicación y ajustes finales.

## 5. Resultados Esperados
Los participantes serán capaces de:
- Implementar un modelo de análisis de sentimientos para textos extensos.
- Desarrollar una interfaz gráfica efectiva para la interacción del usuario.
- Integrar funcionalidades de redes sociales y aplicar filtros de publicación basados en análisis de sentimientos (tambien puede ser fake/mock).
- Reporte de resultados y codigo (latex/git)

